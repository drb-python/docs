=================================
Data Request Broker Documentation
=================================
---------------------------------
Python Data Request Broker (DRB)
---------------------------------

.. image:: https://pepy.tech/badge/drb/month
    :target: https://pepy.tech/project/drb
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: Python Version Support Badge

-------------------

.. _DOM: https://dom.spec.whatwg.org

Drb is a library dedicated to simplify the access to the data with a unique and as simple as possible interface. This interface is based on hierarchy of nodes inherited from DOM_ (Document Object Model). The organization of this hierarchy is defined by the underlying implementation.

Organization of the project
===========================

the project structure is organized as following:
.. _drb : https://gitlab.com/drb-python\ /drb


.. list-table::
   :widths: 15 15 700
   :header-rows: 1

   * - Path in project
     - Version
     - Description
   * - `/drb <https://gitlab.com/drb-python/drb>`_ (`doc <https://drb-python.gitlab.io/drb>`_)
     - |drb_version|
     -  the implementation of DRB data model. It includes a factory mechanism able to discovers implementations
   * - /impl_/file_ (`doc <https://drb-python.gitlab.io/impl/file>`__)
     - |file_version|
     - implementation allowing DRB entering directory and seeing files.
   * - /impl_/ftp_ (`doc <https://drb-python.gitlab.io/impl/ftp>`__)
     - |ftp_version|
     - implementation allowing DRB browse inside ftp protocol.
   * - /impl_/http_ (`doc <https://drb-python.gitlab.io/impl/http>`__)
     - |http_version|
     - implementation allowing DRB browse inside http protocol
   * - /impl_/image_ (`doc <https://drb-python.gitlab.io/impl/image>`__)
     - |img_version|
     - implementation allowing DRB browsing image metadata and data
   * - /impl_/java_ (`doc <https://drb-python.gitlab.io/impl/java>`__)
     - |java_version|
     - implementation allowing DRB using java DRB nodes
   * - /impl_/json_ (`doc <https://drb-python.gitlab.io/impl/json>`__)
     - |json_version|
     - implementation allowing DRB browsing JSON data structure
   * - /impl_/netcdf_ (`doc <https://drb-python.gitlab.io/impl/netcdf>`__)
     - |netcdf_version|
     - implementation allowing DRB browsing netcdf contents
   * - /impl_/odata_ (`doc <https://drb-python.gitlab.io/impl/odata>`__)
     - |odata_version|
     - implementation allowing DRB to access OData service
   * - /impl_/swift_ (`doc <https://drb-python.gitlab.io/impl/swift>`__)
     - |swift_version|
     - implementation allowing DRB browsing OpenStack Swift service
   * - /impl_/tar_ (`doc <https://drb-python.gitlab.io/impl/tar>`__)
     - |tar_version|
     - implementation allowing DRB browse tar contents
   * - /impl_/webdav_ (`doc <https://drb-python.gitlab.io/impl/webdav>`__)
     - |webdav_version|
     - implementation allowing DRB browing webdav service
   * - /impl_/xml_ (`doc <https://drb-python.gitlab.io/impl/xml>`__)
     - |xml_version|
     - implementation allowing DRB browsing XML content
   * - /impl_/zarr_ (`doc <https://drb-python.gitlab.io/impl/zarr>`__)
     - |zarr_version|
     - implementation allowing DRB browsing zarr dataset content
   * - /impl_/zip_ (`doc <https://drb-python.gitlab.io/impl/zip>`__)
     - |zip_version|
     - implementation allowing DRB browsing and extracting zip content
   * - /xquery_
     - |xquery_version|
     - XQuery is a W3C defined language able to be applied on DRB node
   * - /topics_
     -
     -  the set of structured data definition
   * - /topics_/safe_
     - |safe_version|
     - structured representation of a Safe Product
   * - /topics_/sentinel1_
     - |s1_version|
     - structured representation of a sentinel-1 product
   * - /tools_
     -
     - group gathering all utils tools
   * - /tools_/download_manager_
     - |dm_version|
     - tool based on DRB to manage downloads
   * - /tools_/topic-editor_
     - |te_version|
     - GUI to help topics edition
   * - /samples_
     -
     - Sample group is dedicated to contains from basics to complex samples to demonstrate the usage of DRB
   * - /samples_/samples_
     -
     - A set of basic samples in jupyter format
   * - /samples_/stac_
     - |stac_version|
     - DRB usage sample showing the production of the STAC API (https://stacspec.org/)
   * - /samples_/s2l0tile_
     - |s2l0tl_version|
     - DRB service able to retrieves S2 Level-0 et of granules grouped by L1C Tiles
   * - /samples_/timenode_
     -
     - DRB Sample to manage node with clock events
   * - /docs_
     -
     - this document

Most of these modules comes with their own detailed documentation.



.. _drb:  https://gitlab.com/drb-python/drb
.. _impl:  https://gitlab.com/drb-python/impl
.. _file: https://gitlab.com/drb-python/impl/file
.. _ftp: https://gitlab.com/drb-python/impl/ftp
.. _http: https://gitlab.com/drb-python/impl/http
.. _image: https://gitlab.com/drb-python/impl/image
.. _java: https://gitlab.com/drb-python/impl/java
.. _json: https://gitlab.com/drb-python/impl/json
.. _netcdf: https://gitlab.com/drb-python/impl/netcdf
.. _odata: https://gitlab.com/drb-python/impl/odata
.. _swift: https://gitlab.com/drb-python/impl/swift
.. _tar: https://gitlab.com/drb-python/impl/tar
.. _webdav: https://gitlab.com/drb-python/impl/webdav
.. _xml: https://gitlab.com/drb-python/impl/xml
.. _zarr: https://gitlab.com/drb-python/impl/zarr
.. _zip: https://gitlab.com/drb-python/impl/zip
.. _xquery: https://gitlab.com/drb-python/xquery
.. _topics: https://gitlab.com/drb-python/topics
.. _safe: https://gitlab.com/drb-python/topics/safe
.. _sentinel1: https://gitlab.com/drb-python/topics/sentinel-1
.. _tools: https://gitlab.com/drb-python/tools
.. _download_manager: https://gitlab.com/drb-python/tools/download-manager
.. _topic-editor: https://gitlab.com/drb-python/tools/topic-editor
.. _samples: https://gitlab.com/drb-python/samples
.. _sampless: https://gitlab.com/drb-python/samples/samples
.. _stac: https://gitlab.com/drb-python/samples/stac
.. _s2l0tile: https://gitlab.com/drb-python/samples/s2l0tile
.. _timenode: https://gitlab.com/drb-python/samples/timenode
.. _docs: https://gitlab.com/drb-python/docs


.. |doc_root_url| replace:: https://drb-python.gitlab.io/
