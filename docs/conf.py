# -- Path setup --------------------------------------------------------------
import os
import sys
sys.path.append(os.path.abspath('..'))

import drb
import drb_impl_file
import drb_impl_ftp
import drb_impl_http
import drb_impl_image
#import drb_impl_java
import drb_impl_json
import drb_impl_netcdf
import drb_impl_odata
import drb_impl_swift
import drb_impl_tar
import drb_impl_webdav
import drb_impl_xml
import drb_impl_zarr
import drb_impl_zip
import drb_xquery
import drb_download_manager
import drb_topic_safe
import drb_topic_sentinel1

# -- Project information -----------------------------------------------------
project = 'Data Request Broker'
copyright = '2022, GAEL Systems'
author = 'GAEL Systems Editor'

# The full version, including alpha/beta/rc tags
version = drb.__version__
release = drb.__version__

drb_version = drb.__version__
drb_release = drb.__version__
file_version = drb_impl_file.__version__
ftp_version = drb_impl_ftp.__version__
http_version = drb_impl_http.__version__
image_version = drb_impl_image.__version__
#java_version = drb_impl_java.__version__
json_version = drb_impl_json.__version__
netcdf_version = drb_impl_netcdf.__version__
odata_version = drb_impl_odata.__version__
swift_version = drb_impl_swift.__version__
tar_version = drb_impl_tar.__version__
webdav_version = drb_impl_webdav.__version__
xml_version = drb_impl_xml.__version__
zarr_version = drb_impl_zarr.__version__
zip_version = drb_impl_zip.__version__
xquery_version = drb_xquery.__version__
safe_version = drb_topic_safe.__version__
s1_version = drb_topic_sentinel1.__version__
dm_version = drb_download_manager.__version__
#te_version = topic_editor.__version__
#stac_version = stac.__version__
#s2l0tl_version = s2l0tile.__version__

rst_epilog = f'''
.. |drb_version| replace:: {drb_version}
.. |file_version| replace:: {file_version}
.. |ftp_version| replace:: {ftp_version}
.. |http_version| replace:: {http_version}
.. |img_version| replace:: {image_version}
.. |json_version| replace:: {json_version}
.. |netcdf_version| replace:: {netcdf_version}
.. |odata_version| replace:: {odata_version}
.. |swift_version| replace:: {swift_version}
.. |tar_version| replace:: {tar_version}
.. |webdav_version| replace:: {webdav_version}
.. |xml_version| replace:: {xml_version}
.. |zarr_version| replace:: {zarr_version}
.. |zip_version| replace:: {zip_version}
.. |xquery_version| replace:: {xquery_version}
.. |dm_version| replace:: {dm_version}
.. |safe_version| replace:: {safe_version}
.. |s1_version| replace:: {s1_version}

.. |te_version| replace:: 0.0.0
.. |java_version| replace:: 1.0.0
.. |stac_version| replace:: 0.0.0
.. |s2l0tl_version| replace:: 0.0.0
'''

# -- General configuration ---------------------------------------------------
extensions = [
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'sphinx_tabs.tabs',
]
templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output --------------------------------------------------
html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static']
html_static_path = []
html_show_sourcelink = False

# -- InterSphinx configuration ------------------------------------------------
intersphinx_mapping = {
    'python': ('https://docs.python.org/', None),
}
# -- Napoleon configuration ---------------------------------------------------
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = True
# -- Autodoc configuration ----------------------------------------------------
autodoc_default_options = {
    'member-order': 'alphabetical',
    'undoc-members': False,
    'exclude-members': '__weakref__, __hash__, __slots__, __dict__,'
                       '__module__, __abstractmethods__, __init__'
}
