.ONESHELL:
PYTHON=python3
PIP=pip3
PROJECT=docs

ifeq (, $(shell which $(PYTHON)))
 $(error "No $(PYTHON) in $(PATH), consider doing apt-get install python3.6+")
endif

all: docs
.PHONY: all

venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || $(PYTHON) -m venv venv
	. venv/bin/activate
	$(PIP) install wheel
	$(PYTHON) -m pip install -Ur requirements.txt --no-cache-dir
	touch venv/bin/activate

clean:
	@rm -rf public

docs: public venv
	. venv/bin/activate
	sphinx-build -b html docs public

public:
	mkdir -p $@


